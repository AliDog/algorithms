package at.alidog.sorter;

import java.util.ArrayList;
import java.util.List;

import at.alidog.algorithms.Bubble;
import at.alidog.algorithms.Selection;

public class Main {

	public static void main(String[] args) {

		Sorter sorter1 = new Sorter();
		
		List<Integer> list1 = new ArrayList<Integer>();
		List<Integer> list2 = new ArrayList<Integer>();
		
		list1 = sorter1.getRandomArray();
		list2 = sorter1.getRandomArray();
		
		
		System.out.println(list1);
		
		Bubble bubble = new Bubble();
		
		list1= bubble.doSort(list1);
		
		System.out.println(list1);
		
		
		System.out.println(list2);
		
		Selection selection = new Selection();
		
		list2 = selection.doSort(list2);
		
		
		System.out.println(list2);
		
	}

}
