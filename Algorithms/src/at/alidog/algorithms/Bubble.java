package at.alidog.algorithms;

import java.util.List;

public class Bubble implements Algorithm {

	@Override
	public List<Integer> doSort(List<Integer> randomArray) {
		
		
		boolean done = false;
		while(done == false){
			done = true;
			for(int x=1; x < randomArray.size(); x++) {
				
				int digit1 = randomArray.get(x-1);
				int digit2 = randomArray.get(x);
		
				
				if(digit1 > digit2) {
				randomArray.set(x-1, digit2);
				randomArray.set(x, digit1);
				done = false;
				}
				
			}
		}
		return randomArray;
	}

	
	
	
}
