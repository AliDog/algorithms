package at.alidog.algorithms;

import java.util.List;

public interface Algorithm {
public List<Integer> doSort(List<Integer> randomArray);
}
