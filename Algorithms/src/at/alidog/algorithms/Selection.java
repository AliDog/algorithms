package at.alidog.algorithms;

import java.util.List;

public class Selection implements Algorithm {

	@Override
	public List<Integer> doSort(List<Integer> randomArray) {

		for (int i = 0; i < randomArray.size()-1; i++){ 

		    int smallest = i; 
		    for (int j = i+1; j < randomArray.size(); j++) {
		        if (randomArray.get(j) < randomArray.get(smallest)) {
		            smallest = j; 
		        }
		    }
		    int temp = randomArray.get(smallest); 
		    randomArray.set(smallest,randomArray.get(i)); 
		    randomArray.set(i,temp); 
		} 
	
		return randomArray;
	
	}
}
